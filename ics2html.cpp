#include <bits/stdc++.h>

using namespace std;

struct Date {
    int day, time;
};

struct Event {
    string name, location;
    Date start, end;
};

Date strToDate(string s) {
    int J = stoi(s.substr(0,2));
    int K = stoi(s.substr(2,2));
    int m = stoi(s.substr(4,2));
    int q = stoi(s.substr(6,2));
    int hour = stoi(s.substr(9,2));
    int minute = stoi(s.substr(11,2));
    // int time = stoi(s.substr(9,4));
    int time = 2 * hour + minute / 30;

    int dayOW = ( q + ( 13 * m + 13 ) / 5 + K + K/4 + J/4 - 2 * J -2) % 7;

    return {dayOW, time};
}


Event createEvent() {
    string line;
    getline(cin, line);
    Event event;
    while (line.rfind("END:VEVENT",0) != 0) {
        if (line.rfind("SUMMARY:", 0) == 0) {
            event.name = line.substr(line.find(":",0)+1);
        }
        else if (line.rfind("LOCATION:", 0) == 0) {
            event.location = line.substr(line.find(":",0)+1);
        }
        else if (line.rfind("DTSTART", 0) == 0) {
            event.start = strToDate(line.substr(line.find(":",0)+1));
        }
        else if (line.rfind("DTEND", 0) == 0) {
            event.end = strToDate(line.substr(line.find(":",0)+1));
        }
        getline(cin, line);
    }

    return event;
}

void eventsToHTML(vector<Event> events) {
    string week[] = {
        "Montag",
        "Dienstag",
        "Mittwoch",
        "Donnerstag",
        "Freitag",
        "Samstag",
        "Sonntag"
    };

    sort(events.begin(), events.end(), [](Event a, Event b) { 
            return a.start.time > b.start.time; });

    // Style
    cout << "<style>";
    cout << "table, th, td {border: 1px solid black;border-collapse: collapse;}";
    cout << "</style>";

    // Header
    cout << "<table><tr>" << endl;
    cout << "<td>Zeit</td>" << endl;
    for (string day : week) {
        cout << "<th>" << day << "</th>" << endl;
    }
    cout << "</tr>" << endl;
    
    // Body
    int mask[7] = {0};
    queue<Event> table[7];

    Event first = events.back();
    int elements = events.size();
    
    // fill buckets
    while ( events.size() != 0 ) {
        Event top = events.back();
        table[top.start.day].push(top);
        events.pop_back();
    }

    // generate table; empty buckets
    bool isMaskFull = false;
    for (int t = first.start.time; isMaskFull || elements > 0; t++) {
        isMaskFull = false;

        cout << "<tr><td>" << t/2 << ":" << t%2 * 30 << "</td>" << endl;

        for (int i = 0; i < 7; i++) {

            if ( mask[i] > 0 ) {
                mask[i]--; 
                isMaskFull = true;
                // for (int a : mask) cout << a << ' ';
                // cout << endl;
                continue;
            }

            if (table[i].empty()) {cout << "<td></td>" << endl; continue;}
            Event active = table[i].front();
            if ( active.start.time != t ) {cout << "<td></td>" << endl; continue;}

            mask[i] = active.end.time - active.start.time;
            cout << "<td rowspan='" << mask[i] + 1 << "'>";
            cout << "<span class=activity>";
            cout << active.name;
            cout << "</span><br>";
            cout << "<span class=location>";
            cout << active.location;
            cout << "</span>";
            cout << "</td>" << endl;
            table[i].pop();
            elements--;
        }

        cout << "</tr>" << endl;
    }

    cout << "</table>";   
}

int main() {
    string line;
    vector<Event> events;
    
    // search for events
    while ( cin >> line ) {
        if ( line == "BEGIN:VEVENT") {
            events.push_back(createEvent());
        }
    }

    eventsToHTML(events);
}
