# ics2html

> A tool to convert ics calendars to html time tables

## Usage

```sh
cat calendar.ics | ./ics2html > table.html
```

Activities and locations have classes that can be styled with CSS. A style
example can be seen in `./style.css`. 

## Installation

```sh
g++ ics2html.cpp -o ics2html
```

You can add the resulting binary to your `$PATH` to start it from any directory. 

## License

This project is licensed under the GPL-3 License - see the `LICENSE` file for details
